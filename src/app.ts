// cSpell:ignore signup, lokijs

import express from 'express';
import session from 'express-session';
import bodyParser from 'body-parser';
import logger from './util/logger';
import bluebird from 'bluebird';
import loki from 'lokijs';
import fileExists from 'file-exists';

// Routes
import singles from './routes/singles';
import doubles from './routes/doubles';
import home from './routes/home';

// Create Express server
const app = express();

// Create database
const db = new loki('brackets.json', {
  autoload: true,
  autosave: true,
  autosaveInterval: 4000,
});

const TOInfo = db.addCollection('TOInformation');

// Express configuration
app.set('port', 8080);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * Primary app routes.
 */

app.get('/', (req, res) => {
  res.send('Hello world');
});

app.use('/bracket/singles', singles);
// app.use('/bracket/doubles', doubles);

export {app, db, TOInfo};