import { Request, Response } from 'express';
import { EventService } from '../service/EventService';

const eventService = new EventService();

export class EventResource {
  createEvent(req: Request, res: Response): void {
    const eventName: string = req.body.eventName;
    const eventFormat: string = req.body.eventFormat;

    eventService.createBracket(eventName, eventFormat);
  }
}